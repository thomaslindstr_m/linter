#! /usr/bin/env node

const path = require('path');
const {exec} = require('child_process');
const shellEscape = require('shell-escape');
const chalk = require('chalk');
const ora = require('ora');
const {timeLogger} = require('@amphibian/logger');
const pkg = require('./package.json');

const isUsedAsCli = require.main === module;

function lint(directory) {
    timeLogger.log('Linting', chalk.grey(directory), '...\n');

    return new Promise((resolve, reject) => {
        const stylelintSpinner = ora(`Linting ${chalk.blue('*.css')} ...`).start();
        const stylelint = exec([
            `stylelint --config ${path.resolve(__dirname, '.stylelintrc.json')}`,
            shellEscape([`${directory}/**/*.css`])
        ].join(' '));

        let stdout = '';
        let stderr = '';

        stylelint.stdout.on('data', (data) => {
            stdout += data;
        });

        stylelint.stderr.on('data', (data) => {
            stderr += data;
        });

        stylelint.on('exit', (code) => {
            if (code === 0) {
                resolve();
                stylelintSpinner.succeed(`${chalk.green('*.css')} linted.`);
            } else {
                const error = new Error('*.css contain issues.');
                error.stack = stderr;
                reject(error);

                stylelintSpinner.fail(`${chalk.red('*.css')} contain issues.`);
            }

            if (stdout) {
                console.log(stdout);
            }

            if (stderr) {
                console.log(stderr);
            }
        });
    }).then(() => new Promise((resolve, reject) => {
        const eslintSpinner = ora(`Linting ${chalk.blue('*.js(x)')} ...`).start();
        const eslint = exec([
            'eslint',
            `--config ${path.resolve(__dirname, './.eslintrc.json')}`,
            '--format=./node_modules/eslint-formatter-pretty',
            shellEscape([directory]),
            '--ext .js,.jsx'
        ].join(' '));

        let stdout = '';
        let stderr = '';

        eslint.stdout.on('data', (data) => {
            stdout += data;
        });

        eslint.stderr.on('data', (data) => {
            stderr += data;
        });

        eslint.on('exit', (code) => {
            if (code === 0) {
                resolve();
                eslintSpinner.succeed(`${chalk.green('*.js(x)')} linted.`);
            } else {
                const error = new Error('*.js(x) contain issues.');
                error.stack = stderr;
                reject(error);

                eslintSpinner.fail(`${chalk.red('*.js(x)')} contain issues:`);
            }

            if (stdout) {
                console.log(stdout);
            }

            if (stderr) {
                console.log(stderr);
            }
        });
    })).then(() => {
        timeLogger.log('🎉 ', chalk.green('Good to go!'));
    }).catch((error) => {
        timeLogger.log('🙈 ', chalk.red('Fix the issues and try again.'));

        if (isUsedAsCli) {
            process.exit(1);
        } else {
            throw error;
        }
    });
}

if (isUsedAsCli) {
    const cwd = path.resolve(process.cwd(), process.argv[2] || '.');

    timeLogger.log('🦎', chalk.magenta(' @amphibian/linter'), chalk.grey(`(v${pkg.version})`));
    process.env.FORCE_COLOR = 1;

    lint(cwd);
}

module.exports = lint;
