# linter

easily lint an entire project with these opinionated rules for `JavaScript` and `CSS`

```
npm install @amphibian/linter
```

## Why, and what it does

Linting sucks. Or, rather, setting it up sucks. It's time consuming and requires direct attention for every new project.

This module solves that.

Linting code is crucial to avoid fatigue. So why get fatigued before you've even started writing?

## Usage

Just install this module and add this as a `npm script`:

```json
{
    "scripts": {
        "lint": "amphibian-linter ."
    }
}
```

Done? Cool! That's it. Really. Run `npm run lint` to lint your `CSS` and `JavaScript`.

*Simple.*

## Using it as a module

Beyond simple CLI usage, this module exports a `lint` function that can be required:

```
const lint = require('@amphibian/linter');
lint(__dirname);
```
